package pl.Ola.Spring.repository;

import pl.Ola.Spring.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String > {
    User findByUsername(String username);
}
