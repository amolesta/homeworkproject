package pl.Ola.Spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.Ola.Spring.model.Role;

public interface ProductRepository extends JpaRepository<Role, String> {
}
