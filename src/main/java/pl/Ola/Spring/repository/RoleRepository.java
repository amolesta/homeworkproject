package pl.Ola.Spring.repository;

import pl.Ola.Spring.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, String>{
}
