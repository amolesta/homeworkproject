package pl.Ola.Spring.service;

import pl.Ola.Spring.model.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}
