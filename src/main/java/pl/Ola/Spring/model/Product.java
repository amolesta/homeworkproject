package pl.Ola.Spring.model;

import javax.persistence.*;

@Entity
@Table(name = "product")
public class Product {
    private String id;
    private String productname;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }
}
